FROM python:3.7
USER root
WORKDIR /
SHELL ["/bin/bash", "-c"]
ARG APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1
RUN apt-get update && apt-get install curl gnupg2 -y
RUN echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | tee /etc/apt/sources.list.d/coral-edgetpu.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
RUN apt-get update && apt-get install libedgetpu1-std -y
RUN echo "https://github.com/google-coral/pycoral/releases/download/release-frogfish/tflite_runtime-2.5.0-cp37-cp37m-$(uname -s)_$(uname -m).whl" | tr ['A-Z'] ['a-z'] | xargs pip install
RUN pip install jupyterlab matplotlib

WORKDIR /home
